//
//  Coordinator.swift
//  NYCSchools
//
//  Created by Kaushik on 9/17/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    // Holds the array of child coordinators
    var childCoordinators: [Coordinator] { get set }
    // navigation controller to manage the view push and pop
    var navigationController: UINavigationController { get set }
    
    func start()
}
