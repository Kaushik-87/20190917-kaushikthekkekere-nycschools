//
//  Storyboarded.swift
//  NYCSchools
//
//  Created by Kaushik on 9/17/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation
import UIKit

// protocol which handles the instantiate the viewcontroller from Storyboard.
protocol Storyboarded {
    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController {

    static func instantiate() -> Self {
        // this gets the full name of the file NYCSchools.SchoolsViewController.
        let fullName = NSStringFromClass(self)
        
        // this splits by the dot and uses everything after, giving "SchoolsViewController"
        let className = fullName.components(separatedBy: ".")[1]
        
        // load our storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // instantiate a view controller with that identifier, and force cast as the type that was requested
        // storyboard ID and the class name should match.
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }

}
