//
//  NetworkRouter.swift
//  NYCSchools
//
//  Created by Kaushik on 9/19/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation

// Enum to decide which endpoints to hit based on the environment.
enum Environment {
    case qa
    case production
}

// Enum values for different HTTPMethod
// We could add all the types like "PUT", "PATCH", "DELETE"
public enum HTTPMethod: String {
    case get        = "GET"
    case post       = "POST"
}

// Request protocol which defines the parameters which are required to construct a URLRequest.
// baseURL
// method - HTTPMethod
// path - to be append to the base URL
// We ca also add headers, body, since here we are just using two GET requests this should be good.
protocol Request {
    var baseURL: URL { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var queryParam: String { get }
}


public typealias TaskCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

// Protocol method which is required to implement the request and response.
protocol NetworkRouter: class {
    func sendRequest( request: Request, completion: @escaping TaskCompletion)
}

// We need a dispatcher
// NetworkRequestDispatcher which uses the URLSession and task to perform the network communication.
// We can use any other for of communication like Alamorfire.
final class NetworkRequestDispatcher: NetworkRouter {
    private var task: URLSessionTask?
    
    // Actual communication implementation using URLSession
    func sendRequest(request: Request, completion: @escaping TaskCompletion) {
        let session = URLSession.shared
        do {
            let request = try self.buildRequest(from: request)
            task = session.dataTask(with: request, completionHandler: { data, response, error in
                completion(data, response, error)
            })
        }catch {
            completion(nil, nil, error)
        }
        self.task?.resume()
    }
    
    // Constructs the URLRequest from the request objects.
    fileprivate func buildRequest(from request: Request) throws -> URLRequest {
        var urlRequest = URLRequest(url: request.baseURL.appendingPathComponent(request.path),
                                    cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                    timeoutInterval: 10.0)
        
        urlRequest.httpMethod = request.method.rawValue
        if var components = URLComponents(url: urlRequest.url!, resolvingAgainstBaseURL: true) {
            components.query = request.queryParam
            urlRequest = URLRequest(url: components.url!, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10.0)
        }
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return urlRequest
    }
}

