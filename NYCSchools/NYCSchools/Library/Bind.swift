//
//  Bind.swift
//  NYCSchools
//
//  Created by Kaushik on 9/18/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//


// Class helps to bind the values, which can be used to bind to the UI elements.
// Using closures we can just set listeners and then once the value is set the same clousre will trigger to update the UI. 
class Bind<T> {

    typealias Listener = (T) -> Void
    var listener: Listener?
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }

}
