//
//  StringUtils.swift
//  NYCSchools
//
//  Created by Kaushik on 9/19/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation
// Simple method to extract the substring until the provided character. 
extension String {

    func subString(_ beforeString: String) -> String {
        if let stringIndex = self.firstIndex(of: Character(beforeString)) {
            return String(self.prefix(upTo: stringIndex))
        }
        return self
    }

}
