//
//  School.swift
//  NYCSchools
//
//  Created by Kaushik on 9/18/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation

struct School {
    let name: String
    let dbn:  String
    let website: String
    let location: String
    let neighborhood: String
    let overview: String
}

extension School: Decodable {

    enum SchoolCodingKeys: String, CodingKey {
        case dbn            = "dbn"
        case name           = "school_name"
        case website        = "website"
        case location       = "location"
        case neighborhood   = "neighborhood"
        case overview       = "overview_paragraph"
    }
    init(from decoder: Decoder) throws {
        let schoolInfo = try decoder.container(keyedBy: SchoolCodingKeys.self)
        dbn = try schoolInfo.decode(String.self, forKey: .dbn)
        name = try schoolInfo.decode(String.self, forKey: .name)
        website = try schoolInfo.decode(String.self, forKey: .website)
        location = try schoolInfo.decode(String.self, forKey: .location)
        neighborhood = try schoolInfo.decode(String.self, forKey: .neighborhood)
        overview    = try schoolInfo.decode(String.self, forKey: .overview)
    }

}

extension School: CustomStringConvertible {

    var description: String {
        return "\n schoolInfoDBN: \(dbn) \n schoolName: \(name) \n website: \(website) \n address: \(location)"
    }

}
