//
//  SchoolStatistics.swift
//  NYCSchools
//
//  Created by Kaushik on 9/18/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation

struct SchoolStatistics {
    let dbn : String
    let satReadingAvgScore:String
    let satMathAvgScore: String
    let satWritingAvgScore: String
    let schoolName: String
    let noOfTestTakers: String
}

extension SchoolStatistics : Decodable {
    
    enum CodingKeys: String, CodingKey {
        case dbn                        = "dbn"
        case satReadingAvgScore         = "sat_critical_reading_avg_score"
        case satMathAvgScore            = "sat_math_avg_score"
        case satWritingAvgScore         = "sat_writing_avg_score"
        case schoolName                 = "school_name"
        case noOfTestTakers             = "num_of_sat_test_takers"
    }
    
    init(from decoder: Decoder) throws {
        let schoolStatisticsContainer = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try schoolStatisticsContainer.decode(String.self, forKey: .dbn)
        satReadingAvgScore = try schoolStatisticsContainer.decode(String.self, forKey: .satReadingAvgScore)
        satMathAvgScore = try schoolStatisticsContainer.decode(String.self, forKey: .satMathAvgScore)
        satWritingAvgScore = try schoolStatisticsContainer.decode(String.self, forKey: .satWritingAvgScore)
        schoolName = try schoolStatisticsContainer.decode(String.self, forKey: .schoolName)
        noOfTestTakers = try schoolStatisticsContainer.decode(String.self, forKey: .noOfTestTakers)
    }
}

extension SchoolStatistics: CustomStringConvertible {

    var description: String {
        return "schoolStatisticsDBN: \(dbn) \nSchoolName: \(schoolName) \n SAT Reading Score: \(satReadingAvgScore) \n SAT Math Average Score: \(satMathAvgScore) \n SAT Writing Average Score : \(satWritingAvgScore) \n No Of SAT Test Takers : \(noOfTestTakers)"
    }

}
