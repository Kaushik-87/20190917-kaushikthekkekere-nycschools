//
//  SchoolInfoCellTableViewCell.swift
//  NYCSchools
//
//  Created by Kaushik on 9/18/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import UIKit

class SchoolInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var neighborhoodLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func loadSchoolInfo(_ school: School) {
        self.nameLabel.text = school.name
        self.neighborhoodLabel.text = school.neighborhood
    }

}
