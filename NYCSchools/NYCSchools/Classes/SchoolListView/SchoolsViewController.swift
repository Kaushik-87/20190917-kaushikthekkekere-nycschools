//
//  ViewController.swift
//  NYCSchools
//
//  Created by Kaushik on 9/17/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import UIKit

class SchoolsViewController: UIViewController, Storyboarded {

    @IBOutlet private weak var schoolsListTableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: SchoolsListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
        setUpViewModel()
    }
    
    // add the basic UI related to code here
    private func setupUI() {
        // this is to make the cell grow automaticaaly based on its content.
        schoolsListTableView.rowHeight = UITableView.automaticDimension
        schoolsListTableView.estimatedRowHeight = 50
        title = "NYC Schools"        
    }
    
    private func setUpViewModel() {
        viewModel.isLoading.bind {
            self.activityIndicator.isHidden = !$0
            $0 ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }
        viewModel.reloadView = { [weak self] in
            DispatchQueue.main.async {
                self?.schoolsListTableView.reloadData()
            }
        }
    }

}

extension SchoolsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfSchools
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolInfoTableViewCell", for: indexPath) as? SchoolInfoTableViewCell else {
            fatalError("Could not initialize the cell")
        }
        cell.loadSchoolInfo(viewModel.schoolAtIndex(indexPath.row))
        return cell
    }

}

extension SchoolsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectSchool(indexPath.row)
    }

}
