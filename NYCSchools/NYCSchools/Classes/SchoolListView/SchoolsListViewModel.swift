//
//  SchoolsViewModel.swift
//  NYCSchools
//
//  Created by Kaushik on 9/17/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation
import UIKit

typealias SchoolsList = [School]

class SchoolsListViewModel {
    
    // we need the reference of the app coordinator
    private var schoolsList: SchoolsList
    private var service: NYCSchoolsService
    weak var parentCoordinator: AppCoordinator?
    var isLoading: Bind<Bool> = Bind(false)
    // closure to update the UI once data is fetched
    var reloadView: (()->())?
    
    init(service: NYCSchoolsService = NYCSchoolsService()) {
        self.service = service
        schoolsList = [School]()
    }
    
    func showAllSchools() {
        isLoading.value = true
        service.fetchAllSchoolsInNYC {[weak self] (schools, errorString) in
            DispatchQueue.main.async {
                self?.isLoading.value = false
                if errorString != nil {
                    // We can prompt an alert for the user regarding the error
                    print("Error --- \(String(describing: errorString))")
                    return
                }
                
                guard let schools = schools else  {
                    // We can prompt an alert for the user regarding the error

                    print("Error while fetching schools")
                    return
                }
                self?.schoolsList = schools
                self?.reloadView?()
            }
        }
    }
    
    var numberOfSchools: Int {
        return schoolsList.count
    }
    
    func schoolAtIndex(_ index: Int) -> School {
        return schoolsList[index]
    }
    
    func didSelectSchool(_ index: Int) {
        parentCoordinator?.showSchoolDetails(schoolsList[index])
    }

}
