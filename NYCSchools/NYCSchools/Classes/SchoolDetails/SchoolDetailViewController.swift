//
//  SchoolDetailViewController.swift
//  NYCSchools
//
//  Created by Kaushik on 9/18/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController, Storyboarded {

    @IBOutlet private weak var schoolTitle: UILabel!
    @IBOutlet private weak var mathScoreLabel: UILabel!
    @IBOutlet private weak var readingScoreLabel: UILabel!
    @IBOutlet private weak var writingScoreLabel: UILabel!
    @IBOutlet private weak var noOfTestTakersLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var websiteLabel: UILabel!
    @IBOutlet private weak var overviewParagraphLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: SchoolDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Details"
        setUpUI()
        setupUIBinding()
    }
    
    private func setUpUI() {
        addressLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addressTapped(sender:))))
        websiteLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SchoolDetailViewController.websiteTapped(sender:))))
    }

    private func setupUIBinding() {
        viewModel.schoolName.bind {
            self.schoolTitle.text = $0
        }
        viewModel.schoolAddress.bind {
            self.addressLabel.text = $0
        }
        viewModel.schoolWebSite.bind {
            self.websiteLabel.text = $0
        }
        viewModel.schoolOverview.bind {
            self.overviewParagraphLabel.text = $0
        }
        viewModel.schoolSATMathScore.bind {
            self.mathScoreLabel.text = $0
        }
        viewModel.schoolSATReadingScore.bind {
            self.readingScoreLabel.text = $0
        }
        viewModel.schoolSATWritingScore.bind {
            self.writingScoreLabel.text = $0
        }
        viewModel.schoolSATNoOfTestTakers.bind {
            self.noOfTestTakersLabel.text = $0
        }
        viewModel.isLoading.bind {
            self.activityIndicator.isHidden = !$0
            $0 ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }
    }
    
    @objc private func addressTapped(sender: Any) {
        viewModel.didTapOnAddress()
    }
    @objc private func websiteTapped(sender: UITapGestureRecognizer) {
        viewModel.didTapOnWebsite()
    }

}
