//
//  SchoolDetailViewModel.swift
//  NYCSchools
//
//  Created by Kaushik on 9/18/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation
import UIKit

class SchoolDetailViewModel {
    
    private var service: NYCSchoolsService
    private let school: School
    private var schoolStatistics: SchoolStatistics?
    weak var parentCoordinator: AppCoordinator?
    
    // Setup default values
    var schoolName: Bind<String> = Bind("")
    var schoolAddress: Bind<String> = Bind("")
    var schoolWebSite: Bind<String> = Bind("")
    var schoolOverview: Bind<String> = Bind("")
    var schoolSATMathScore: Bind<String> = Bind("")
    var schoolSATReadingScore: Bind<String> = Bind("")
    var schoolSATWritingScore: Bind<String> = Bind("")
    var schoolSATNoOfTestTakers: Bind<String> = Bind("")
    
    var isLoading: Bind<Bool> = Bind(false)
    
    init(_ school: School, service: NYCSchoolsService = NYCSchoolsService()) {
        self.school = school
        self.service = service
        schoolName.value = school.name
        // Location comes with coordinates which we do not want to show to the user. We use our string extension to get the substring until location coordinate start with "(".
        schoolAddress.value = school.location.subString("(")
        schoolOverview.value = school.overview
        schoolWebSite.value = school.website
    }
    
    // Fetches school statistics for given school
    func showSchoolDetails() {
        isLoading.value = true
        service.fetchSchoolDetails(school) { [weak self] (schoolStats, error) in
            DispatchQueue.main.async {
                // We do not need to retunr in case of error coz user can still navigate to detail screen which displays School details with SAT scores as "-NA-".
                self?.isLoading.value = false
                if error != nil {
                    print("Error while fetching details for the school = \(String(describing: self?.school.description)))")
                }
           
                // once we have the value we just set the values to the properties using binding.
                self?.schoolStatistics = schoolStats
                self?.schoolSATMathScore.value = schoolStats?.satMathAvgScore ?? "-NA-"
                self?.schoolSATReadingScore.value = schoolStats?.satReadingAvgScore ?? "-NA-"
                self?.schoolSATWritingScore.value = schoolStats?.satWritingAvgScore ?? "-NA-"
                self?.schoolSATNoOfTestTakers.value = schoolStats?.noOfTestTakers ?? "-NA-"
            }
        }
    }
    
    // Here we can get the geo location coordinates and open that in Maps applications.
    func didTapOnAddress() {
    }
    
    // Here we can handle the URL and open in external safari or in app safari controller.
    func didTapOnWebsite() {
    }

}
