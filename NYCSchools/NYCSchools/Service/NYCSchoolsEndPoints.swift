//
//  NYCSchoolEndPoints.swift
//  NYCSchools
//
//  Created by Kaushik on 9/19/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation

// Enum which could holds different endpoints.
public enum NYCSchoolsEndPoints {
    case schoolsList(_ filteredParameters: String)
    case schoolDetails(_ dbn: String)
}

// We need the Request
// Implements the Request protocol to provide all the necessary values required for the URLRequest.
extension NYCSchoolsEndPoints: Request {

    private var environmentBaseURL: String {
        switch NYCSchoolsService.environment {
        case .production:
            return kNYCSchoolsBaseURL
        case .qa:
            // Just for the demo purpose
            return "https://data.cityofnewyork.us/resource/"
        }
    }
    var baseURL: URL {
        guard let baseURL = URL(string: environmentBaseURL) else {
            fatalError("Failed to construct Base URL")
        }
        return baseURL
    }
    
    var method: HTTPMethod {
        // We can decide based on the endpoints
        return .get
    }
    
    var path: String {
        // Return the path based on the endpoints.
        switch self {
        case .schoolsList(_):
            return "s3k6-pzi2.json"
        case .schoolDetails(_):
            return "f9bf-2cp4.json"
        }
    }
    
    var queryParam: String {
        switch self {
        case .schoolsList(let filteredParameters):
            return "\(filteredParameters)"
        case .schoolDetails(let dbn):
            return "dbn=\(dbn)"
        }
    }
}

