//
//  NYCSchoolsServices.swift
//  NYCSchools
//
//  Created by Kaushik on 9/17/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation

struct NYCSchoolsService {
    static let environment = Environment.production
    
    // We need a apiClient object which sends the request and handles the response.
    // This is the object which implements the NetworkRouter protocol.
    // Right now apiClient is accessible, so that we can set the mock networkRouter during the testing.
    var apiClient: NetworkRouter = NetworkRequestDispatcher()
    
    func fetchAllSchoolsInNYC(completion: @escaping (_ schools: [School]?,_ error: NYCSchoolsServiceError?)->()) {
        
        // sendRequest and process the response.
        // Get the request object from the EndPoints enum.
        // we can still make more dynamic to provide as query parameter. 
        apiClient.sendRequest(request: NYCSchoolsEndPoints.schoolsList("state_code=NY&$order=school_name ASC&$select=school_name,dbn,website,location,neighborhood,overview_paragraph")) { (data, response, error) in
            
            // In case of error send back to the caller by adding the Service layer error object.
            if error != nil {
                completion(nil, NYCSchoolsServiceError.network)
                return
            }
            
            if let response = response as? HTTPURLResponse {
                // Check whether we have a valid response
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    
                    // Process data only if we have the success response.
                    guard let responseData = data else {
                        completion(nil, NYCSchoolsServiceError.noData)
                        return
                    }
                    do {
                        print(responseData)
                        let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        print(jsonData)
                        let schoolsListResponse = try JSONDecoder().decode([School].self, from: responseData)
                        completion(schoolsListResponse, nil)
                    }catch {
                        
                        // Send the parsing error to the caller
                        print(error)
                        completion(nil, NYCSchoolsServiceError.unableToDecode)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
                
            } else {
                completion(nil, NYCSchoolsServiceError.noData)
            }
        }
    }
    
    func fetchSchoolDetails(_ school: School, completion: @escaping (_ schoolStatistics: SchoolStatistics?, _ error: NYCSchoolsServiceError?)->()) {
        
        // sendRequest and process the response.
        // Get the request object from the EndPoints enum.
        apiClient.sendRequest(request: NYCSchoolsEndPoints.schoolDetails(school.dbn)) { (data, response, error) in
            
            // In case of error send back to the caller by adding the Service layer error object.
            if error != nil {
                completion(nil, NYCSchoolsServiceError.network)
                return
            }
            
            if let response = response as? HTTPURLResponse {
                // Check whether we have a valid response
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    
                    // Process data only if we have the success response.
                    guard let responseData = data else {
                        completion(nil, NYCSchoolsServiceError.noData)
                        return
                    }
                    do {
                        print(responseData)
                        if let jsonData = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) {
                            print(jsonData)
                            let schoolStatisticsResponse = try JSONDecoder().decode([SchoolStatistics].self, from: responseData)
                            print(schoolStatisticsResponse)
                            completion(schoolStatisticsResponse.first, nil)
                        } else {
                            completion(nil, NYCSchoolsServiceError.noData)
                        }
                    }catch {
                        
                        // Send the parsing error to the caller
                        print(error)
                        completion(nil, NYCSchoolsServiceError.unableToDecode)
                    }
                    
                case .failure(let error):
                    completion(nil, error)
                }
                
            } else {
                 completion(nil, NYCSchoolsServiceError.noData)
            }
        }
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> NetworkResult{
        // Check for the proper status code of the HTTPURLResponse object before processing the result.
        switch response.statusCode {
        case 200...299: return .success
        default: return .failure(NYCSchoolsServiceError.failed)
        }
    }

}

// Network result enum which will be either success or failure.
enum NetworkResult {
    case success
    case failure(NYCSchoolsServiceError)
}

// Enum values along with the localized description.
enum NYCSchoolsServiceError: Error {
    case network
    case noData
    case unableToDecode
    case badRequest
    case failed
    var localizedDescription: String {
        switch self {
        case .network:
            return kNetworkServerError
        case .noData:
            return kNetworkNoDataError
        case .badRequest:
            return kNetworkBadRequestError
        case .failed:
            return kNetworkFailedError
        case .unableToDecode:
            return kNetworkUnableToDecodeError
        }
    }
}
