//
//  Constants.swift
//  NYCSchools
//
//  Created by Kaushik on 9/19/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation

let kNYCSchoolsBaseURL = "https://data.cityofnewyork.us/resource/"

let kNetworkServerError             = "Could not connect to Server."
let kNetworkNoDataError             = "Response returned with no data to decode."
let kNetworkBadRequestError         = "Bad request"
let kNetworkFailedError             = "Network request failed."
let kNetworkUnableToDecodeError     = "We could not decode the response."
