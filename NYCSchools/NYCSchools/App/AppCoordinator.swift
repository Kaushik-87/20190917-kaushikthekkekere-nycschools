//
//  AppCoordinator.swift
//  NYCSchools
//
//  Created by Kaushik on 9/17/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {

    // We can maintain array of all the child coordinators as and when we add new flows.
    // for now we can just use single coordinator.
    var childCoordinators = [Coordinator] ()
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let schoolListViewModel = SchoolsListViewModel()
        let schoolsVC           = SchoolsViewController.instantiate()
        schoolListViewModel.parentCoordinator = self
        schoolsVC.viewModel     = schoolListViewModel
        schoolListViewModel.showAllSchools()
        navigationController.pushViewController(schoolsVC, animated: false)
    }
    
    func showSchoolDetails(_ school: School) {
        let schoolDetailViewModel   = SchoolDetailViewModel(school)
        let schoolDetailsVC         = SchoolDetailViewController.instantiate()
        schoolDetailsVC.viewModel   = schoolDetailViewModel
        schoolDetailViewModel.showSchoolDetails()
        navigationController.pushViewController(schoolDetailsVC, animated: true)
    }
    
}
