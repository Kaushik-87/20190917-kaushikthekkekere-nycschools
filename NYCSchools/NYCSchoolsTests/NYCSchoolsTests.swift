//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Kaushik on 9/17/19.
//  Copyright © 2019 Kaushik. All rights reserved.
//

import XCTest
@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testNYCSchoolServiceError() {
        var mockService = NYCSchoolsService()
        mockService.apiClient = MockNetworkLayer()
        let expectation = XCTestExpectation(description: "schools list")
        mockService.fetchAllSchoolsInNYC { (schools, error) in
            expectation.fulfill()
            if error != nil {
                
            } else {
                XCTFail("Data is not expected in case of wrong path")
            }
        }
        wait(for: [expectation], timeout: 100)
    }
    
    func testNYCSchoolServiceSuccessFullResponce()  {
        var mockService = NYCSchoolsService()
        mockService.apiClient = MockNetworkLayer2()
        let expectation = XCTestExpectation(description: "schools")
        mockService.fetchAllSchoolsInNYC { (schools, error) in
            expectation.fulfill()
            if error != nil {
                XCTFail("Error is not expected")
            } else {
                XCTAssertTrue(schools?.count == 2, "Expected no of schools is 2!!")
            }
        }
        wait(for: [expectation], timeout: 100)
    }
    
    func testNYCSchoolServiceValidateStatusCode()  {
        var mockService = NYCSchoolsService()
        mockService.apiClient = MockNetworkLayer3()
        let expectation = XCTestExpectation(description: "schools")
        mockService.fetchAllSchoolsInNYC { (schools, error) in
            expectation.fulfill()
            if error == nil {
                XCTFail("Error is expected")
            }
            if schools != nil {
                XCTFail("School list is not expected!!")
            }
        }
        wait(for: [expectation], timeout: 100)
    }
    
    func testNYCSchoolServiceValidateJSON()  {
        var mockService = NYCSchoolsService()
        mockService.apiClient = MockNetworkLayer4()
        let expectation = XCTestExpectation(description: "schools")
        mockService.fetchAllSchoolsInNYC { (schools, error) in
            expectation.fulfill()
            XCTAssertTrue(error == NYCSchoolsServiceError.unableToDecode, "Error is expected")
        }
        wait(for: [expectation], timeout: 100)
    }
}



// Below code demonstrate how we can create our own Mock networklayer using NetworkRouter Protocol for unit testing.
// And then set the mock network layer to the service.
// We can add more tests by mocking different network scenarios.
//
class MockNetworkLayer: NetworkRouter {
    func sendRequest(request: Request, completion: @escaping TaskCompletion) {
        let errorTemp = NSError(domain:"network", code:500, userInfo:nil)
        completion(nil,nil,errorTemp)
    }
}
class MockNetworkLayer2: NetworkRouter {
    func sendRequest(request: Request, completion: @escaping TaskCompletion) {
        let mockResponseString = "[{\"school_name\":\"The American School\",\"dbn\":\"02M047\",\"website\":\"www.47aslhs.org\",\"location\":\"223 East 23rd Street, Manhattan NY 10010 (40.738374, -73.981329)\",\"neighborhood\":\"Gramercy\",\"overview_paragraph\":\"the American Sign Language and English High School, we believe that all students have the right to learn in an inclusive, academically rigorous environment. Our elective courses challenge our studentsÂ’ creativity and our graduates emerge proficient in American Sign Language (ASL). Our school community nurtures responsible citizens of the world, addressing all aspects of young adult development by providing the necessary individual supports for our students to be college and career ready.\"},{\"school_name\":\"The American School\",\"dbn\":\"02M047\",\"website\":\"www.47aslhs.org\",\"location\":\"223 East 23rd Street, Manhattan NY 10010 (40.738374, -73.981329)\",\"neighborhood\":\"Gramercy\",\"overview_paragraph\":\"the American Sign Language and English High School, we believe that all students have the right to learn in an inclusive, academically rigorous environment. Our elective courses challenge our studentsÂ’ creativity and our graduates emerge proficient in American Sign Language (ASL). Our school community nurtures responsible citizens of the world, addressing all aspects of young adult development by providing the necessary individual supports for our students to be college and career ready.\"}]"
        let data = mockResponseString.data(using: .utf8)!
        completion(data, HTTPURLResponse(url: request.baseURL, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
    }
}

class MockNetworkLayer3: NetworkRouter {
    func sendRequest(request: Request, completion: @escaping TaskCompletion) {
        let mockResponseString = "[{\"school_name\":\"The American School\",\"dbn\":\"02M047\",\"website\":\"www.47aslhs.org\",\"location\":\"223 East 23rd Street, Manhattan NY 10010 (40.738374, -73.981329)\",\"neighborhood\":\"Gramercy\",\"overview_paragraph\":\"the American Sign Language and English High School, we believe that all students have the right to learn in an inclusive, academically rigorous environment. Our elective courses challenge our studentsÂ’ creativity and our graduates emerge proficient in American Sign Language (ASL). Our school community nurtures responsible citizens of the world, addressing all aspects of young adult development by providing the necessary individual supports for our students to be college and career ready.\"},{\"school_name\":\"The American School\",\"dbn\":\"02M047\",\"website\":\"www.47aslhs.org\",\"location\":\"223 East 23rd Street, Manhattan NY 10010 (40.738374, -73.981329)\",\"neighborhood\":\"Gramercy\",\"overview_paragraph\":\"the American Sign Language and English High School, we believe that all students have the right to learn in an inclusive, academically rigorous environment. Our elective courses challenge our studentsÂ’ creativity and our graduates emerge proficient in American Sign Language (ASL). Our school community nurtures responsible citizens of the world, addressing all aspects of young adult development by providing the necessary individual supports for our students to be college and career ready.\"}]"
        let data = mockResponseString.data(using: .utf8)!
        completion(data, HTTPURLResponse(url: request.baseURL, statusCode: 300, httpVersion: nil, headerFields: nil), nil)
    }
}

class MockNetworkLayer4: NetworkRouter {
    func sendRequest(request: Request, completion: @escaping TaskCompletion) {
        let mockResponseString = "[{\"school_name\":\"The American School\",\"dbn\":\"02M047\",\"website\":\"www.47aslhs.org\",\"location\":\"223 East 23rd Street, Manhattan NY 10010 (40.738374, -73.981329)\",\"neighborhood\":\"Gramercy\",\"overview_paragraph\":\"the American Sign Language and English High School, we believe that all students have the right to learn in an inclusive, academically rigorous environment. Our elective courses challenge our studentsÂ’ creativity and our graduates emerge proficient in American Sign Language (ASL). Our school community nurtures responsible citizens of the world, addressing all aspects of young adult development by providing the necessary individual supports for our students to be college and career ready.\"]"
        let data = mockResponseString.data(using: .utf8)!
        completion(data, HTTPURLResponse(url: request.baseURL, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
    }
}

/// Creating a mock request object using Request protocol
class MockRequest1: Request {
    var queryParam: String {
       return "state_code=NY&$order=school_name ASC&$select=school_name,dbn,website,location,neighborhood,overview_paragraph"
    }
    
    private var environmentBaseURL: String {
        switch NYCSchoolsService.environment {
        case .production:
            return "https://data.cityofnewyork.us/resource/"
        case .qa:
            return "https://data.cityofnewyork.us/resource/"
        }
    }
    var baseURL: URL {
        guard let baseURL = URL(string: environmentBaseURL) else {
            fatalError("Failed to construct Base URL")
        }
        return baseURL
    }
    
    var method: HTTPMethod {
        return .get
    }
    // Here we test if we miss to add the proper path
    var path: String {
        return ""
    }
}
