# 20190917-KaushikThekkekere-NYCSchools

  Project name    - NYC Schools  
  Target platform - Universal (iPhone and iPad)  
  Xcode version   - 10.3  
  Language        - Swift 5  
  Author          - Kaushik Thekkekere  

**Description:** 
        This project is developed as part of the coding challenge. App lists down all the schools in NYC and also capable of displaying the details of selected school. 
        Devloped this by giving more importance on demonstrating clean architecture, testability and code reusability.
        It has two screens 
        
        
        Screen 1 - NYC Schools  
                        Lists all the schools using the API - https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2   
                        Display the list using TableView   
                        Each row displays school name and neighborhood it is located.
                        
                        
        Screen 2 - Details 
                        Tapping on each school will navigate to this screen of detail  
                        Displays - SAT score, Math, Reading, Writing and also No of Test Takers  
                        Displays - Address, Website and overview about the school   
                        
**Design pattern** - Used mainly **MVVM + Coordinator** pattern. This patterns helps to maintain the more modular code. Here the view controllers are kept as thin as possible.
                Coordinator pattern helps in taking the responsiblity of controlling the app flow. View model is responsible for handling the logic and communicating with the coordinator.
            
  **Features implmented**
  
  
      * Autolayout with safe area  
      * Portrait and landscape   
      * Dynamic Type to support large texts  
      * Unit tests using mock network layer  
      * ViewModel to view binding  
      * Protocol oriented Network layer to help mocking while unit testing 
      * Handled error and failure scenarios
      * Details view has scroll view which enables scrolling for smaller devices.
      * Activity indicator to show network activity 
      * No third party frameworks are being used  
  
 **Scope for improvements**  
 
    * Add more Unit tests and UItests  
    * Display errors using AlertController  
    * Intial plan was to create framework for the files under Library (could not finish in time)  
    * Along with the list view, Schools could be shown in a Map view (using segment controll to switch the view)
    * Pull to refresh the School details. 
    * Can add the school details to the school model to avoid fetching the details everytime. 
                
